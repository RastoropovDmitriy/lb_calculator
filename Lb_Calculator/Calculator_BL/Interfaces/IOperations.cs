﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_BL.Interfaces
{
    interface IOperations
    {
        string Add(string arg1, string arg2);
        string Increase(string arg1, string arg2);
        string Deduct(string arg1, string arg2);
        string Divide(string arg1, string arg2);
        string Consult(string some);
        string Upcast();
    }
}
