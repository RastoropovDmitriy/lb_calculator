﻿

namespace Lb_Calculator.ViewModels
{
    public class CalculatorViewModel : BaseViewModel
    {
        private string _expression;
        private string _memory;
        public KeyboardViewModel Keyboard { get; set; }

        public string Expression
        {
            get { return _expression; }
            set
            {
                _expression = value;
                OnPropertyChanged("Expression");
            }
        }

        public string Memory {
            get
            {
                return _memory;
            }
            set
            {
                _memory = value;
                OnPropertyChanged("Memory");
            }
        }

        public CalculatorViewModel()
        {
            Keyboard = new KeyboardViewModel();

            Keyboard.OnKeyClick += PrintExpression;
        }

        private void PrintExpression(string expression, string memory)
        {
            Expression = expression;
            Memory = memory;
        }
    }
}
