﻿using System.Collections.Generic;
using System.Windows.Input;
using Lb_Calculator.Command;
using Calculator_BL;

namespace Lb_Calculator.ViewModels
{
    public class KeyboardViewModel : BaseViewModel
    {
        private Operation _operation;
        public List<List<string>> KeyList { get; set; }
        public string Expression { get; set; }
        public string Memory { get; set; }
        public string Action { get; set; }
        public ICommand KeyPressCommand { get; set; }

        public delegate void Keyboard(string expression, string memory);

        public event Keyboard OnKeyClick;

        public KeyboardViewModel()
        {
            _operation = new Operation();
            GetValue();

            KeyPressCommand = new RelayCommand(KeyPressAction);
            ((RelayCommand) KeyPressCommand).IsEnabled = true;

            Memory = "0";
            Action = "";
        }

        private void KeyPressAction(object obj)
        {
            var symbol = obj as string;

            switch (symbol)
            {
                case "reset":
                    Expression = "";
                    Memory = "0";
                    Action = "";
                    break;
                case "C":
                    Expression = !string.IsNullOrWhiteSpace(Expression) ? Expression.Substring(0, Expression.Length - 1) : "";
                    break;
                case "=":
                    Expression = Memory;
                    //Expression = _operation.Count(Expression);
                    break;
                case "+":
                    if (Expression == "")
                    {
                        Action = "+";
                    }
                    else
                    {
                        Memory = _operation.Add(Memory, Expression);
                        Expression = "";
                    }
                    break;
                case "-":
                    if (Expression == "")
                    {
                        Action = "-";
                    }
                    break;
                case "*":
                    if (Expression == "")
                    {
                        Action = "*";
                    }
                    break;
                case "/":
                    if (Expression == "")
                    {
                        Action = "/";
                    }
                    break;
                default:
                   Expression += symbol;
                   break;
            }


            var handler = OnKeyClick;
            if(handler != null)
                handler.Invoke(Expression, Memory);
        }

        private void GetValue()
        {
            KeyList = new List<List<string>>()
            {
                new List<string>{"7", "8", "9", "+", "C", "reset"},
                new List<string>{"4", "5", "6", "-", null, null},
                new List<string>{"1", "2", "3", "*", null, null},
                new List<string>{"0", ",", "=", "/", null, null}
            };
        }
    }
}
