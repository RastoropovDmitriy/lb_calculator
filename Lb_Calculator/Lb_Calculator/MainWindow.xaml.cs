﻿using Lb_Calculator.ViewModels;
using MahApps.Metro.Controls;

namespace Lb_Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            MainCalculator.DataContext = new CalculatorViewModel();
        }
    }
}
