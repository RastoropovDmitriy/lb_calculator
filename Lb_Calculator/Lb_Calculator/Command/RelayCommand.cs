﻿using System;
using System.Windows.Input;

namespace Lb_Calculator.Command
{
    class RelayCommand : ICommand
    {
        private readonly Action<object> _handler;

        private bool _isEnabled;

        public event EventHandler CanExecuteChanged;


        public RelayCommand(Action<object> handler)
        {
            _handler = handler;
        }

        public bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                if (value == _isEnabled) return;
                _isEnabled = value;
                if (CanExecuteChanged != null)
                {
                    CanExecuteChanged(this, EventArgs.Empty);
                }
            }
        }

        public bool CanExecute(object parameter)
        {
            return IsEnabled;
        }

        public void Execute(object parameter)
        {
            _handler(parameter);
        }
    }
}
